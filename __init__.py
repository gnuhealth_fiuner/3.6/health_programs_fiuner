# -*- coding: utf-8 -*-

from trytond.pool import Pool
from .health_programs_fiuner import *
from .report import *
from .wizard import *



def register():
    Pool.register(
        LecheMaternaMadres,
        LecheMaternaMenores,
        CreateProgramsReportsStart,
        module='health_programs_fiuner', type_='model')
    Pool.register(
        ProgramsLecheReport,
        ProgramsSaludSexualReport,
        ProgramsRemediarReport,
        module='health_programs_fiuner', type_='report')
    Pool.register(
        CreateProgramsReportsWizard,
        module='health_programs_fiuner', type_='wizard')
