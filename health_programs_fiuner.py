# -*- coding: utf-8 -*-

from trytond.model import ModelView, ModelSingleton, ModelSQL, fields
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, date
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Not, Bool, Eval, Equal

__all__ = ['LecheMaternaMadres','LecheMaternaMenores']


class LecheMaternaMadres(ModelSQL,ModelView):
    'Programa de entrega de leche a Madres'
    __name__ = 'programas.leche_materna_madres'

    fecha_control = fields.Date('Fecha de Control', required=True)
    fecha_entrega  = fields.Date('Fecha de Entrega',required=True)
    usuario = fields.Many2One(
        'gnuhealth.patient','Apellido y Nombre', required=True)
    dni = fields.Function(
        fields.Char('DNI'),
        'get_dni', searcher='search_dni')

    def get_dni(self, usuario):
        if self.usuario:
           return self.usuario.name.ref    

    @classmethod
    def search_dni(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('usuario.name.ref', clause[1], value))
        return res

    edad = fields.Function(
        fields.Char('Edad', help="Edad a la fecha de la entrega"),
        'get_patient_age')

    def get_patient_age(self, usuario):
        nac = self.usuario.name.dob
        res = relativedelta(self.fecha_entrega, nac)
        return str(float(res.years) + float(str(res.months/12.0)))

    gestaciones = fields.Integer('Gestaciones', required=True)
    partos = fields.Integer('Partos', required=True)  
    consultas = fields.Integer('Num Consultas', required=True)
    sem_embarazo = fields.Integer('Sem. de embarazo', required=True)
    fpp = fields.Function(fields.Date('FPP'), 'get_fpp')
    
    def get_fpp (self,name):
        dias_falt = (40-self.sem_embarazo)*7
        fpp = self.fecha_control + timedelta(dias_falt)
        return fpp
    
    peso = fields.Float('Peso [kg]', required=True)  
    talla = fields.Float('Talla [cm]', required=True)    
    talla_m = fields.Function(fields.Float('Talla[m]'), 'get_tallam') 
    
    def get_tallam(self, name):
        talla_m = (self.talla)/100
        return talla_m
    
    imc = fields.Function(fields.Float('IMC'), 'get_imc')

    def get_imc(self, name):
        imc = 0
        if self.peso and self.talla:
            imc = float(self.peso) / pow((float(self.talla)/100),2)
        return float(imc)

    diag_nut = fields.Selection([
        (None, ''),
        ('N', 'Normal'),
        ('BP', 'Bajo Peso'),
        ('SP', 'Sobrepeso'),
        ('OB', 'Obesidad'),
        ], 'Diag. Nutricional',
        required=True, sort=False)    
    diag_normal = fields.Function(fields.Char('Diag N'), 'get_diag_normal')
    
    def get_diag_normal(self, name):
        if self.diag_nut == 'N':
            diag_normal = 'X'
        else:
            diag_normal=' '
        return diag_normal

    diag_bp = fields.Function(fields.Char('Diag BP'), 'get_diag_bp')
    
    def get_diag_bp(self, name):
        if self.diag_nut == 'BP':
            diag_bp = 'X'
        else:
            diag_bp=' '
        return diag_bp

    diag_sp = fields.Function(fields.Char('Diag SP'), 'get_diag_sp')
    
    def get_diag_sp(self, name):
        if self.diag_nut == 'SP':
            diag_sp = 'X'
        else:
            diag_sp=' '
        return diag_sp

    diag_ob = fields.Function(fields.Char('Diag OB'), 'get_diag_ob')
    
    def get_diag_ob(self, name):
        if self.diag_nut == 'OB':
            diag_ob = 'X'
        else:
            diag_ob=' '
        return diag_ob

    emb = fields.Selection([
        (None, ''),
        ('N', 'Normal'),
        ('P', 'Problematico'),
        ], 'Embarazo',
        required=True, sort=False)
    emb_normal = fields.Function(fields.Char('Emb N'), 'get_emb_normal')
    
    def get_emb_normal(self, name):
        if self.emb == 'N':
            emb_normal = 'X'
        else:
            emb_normal=' '
        return emb_normal
    
    emb_prob = fields.Function(fields.Char('Emb P'), 'get_emb_prob')
    
    def get_emb_prob(self, name):
        if self.emb == 'P':
            emb_prob = 'X'
        else:
            emb_prob=' '
        return emb_prob   

    cantidad = fields.Integer('Cantidad  [Kg]', required=True)
    
    @staticmethod
    def default_cantidad():
        return 2

    observaciones = fields.Char('Observaciones')


class LecheMaternaMenores(ModelSQL,ModelView):
    'Programa de entrega de leche a Menores'
    __name__ = 'programas.leche_materna_menores'

    wboys_w = [[2.5,2.8,3.0,3.3,3.7,4.0,4.3],[2.6,2.9,3.2,3.5,3.8,4.2,4.5],
           [2.8,3.1,3.4,3.8,4.1,4.5,4.9],[3.1,3.4,3.7,4.1,4.5,4.8,5.2],
           [3.4,3.7,4.0,4.4,4.8,5.2,5.6],[3.6,3.9,4.3,4.7,5.1,5.5,5.9],
           [3.8,4.2,4.5,4.9,5.4,5.8,6.3],[4.1,4.4,4.8,5.2,5.6,6.1,6.5],
           [4.3,4.6,5.0,5.4,5.9,6.3,6.8],[4.4,4.8,5.2,5.6,6.1,6.6,7.1],
           [4.6,5.0,5.4,5.8,6.3,6.8,7.3],[4.8,5.2,5.6,6.0,6.5,7.0,7.5],
           [4.9,5.3,5.7,6.2,6.7,7.2,7.7]]
    wgirls_w = [[2.4,2.7,2.9,3.2,3.6,3.9,4.2],[2.5,2.8,3.0,3.3,3.7,4.0,4.4],
            [2.7,3.0,3.2,3.6,3.9,4.3,4.6],[2.9,3.2,3.5,3.8,4.2,4.6,5.0],
            [3.1,3.4,3.7,4.1,4.5,4.9,5.3],[3.3,3.6,4.0,4.3,4.8,5.2,5.6],
            [3.5,3.8,4.2,4.6,5.0,5.4,5.9],[3.7,4.0,4.4,4.8,5.2,5.7,6.1],
            [3.9,4.2,4.6,5.0,5.5,5.9,6.4],[4.1,4.4,4.7,5.2,5.7,6.1,6.6],
            [4.2,4.5,4.9,5.4,5.8,6.3,6.8],[4.3,4.7,5.1,5.5,6.0,6.5,7.0],
            [4.5,4.8,5.2,5.7,6.2,6.7,7.2]]
    wboys_m = [[2.5,2.8,3.0,3.3,3.7,4.0,4.3],[3.4,3.8,4.1,4.5,4.9,5.3,5.7],
           [4.4,4.7,5.1,5.6,6.0,6.5,7.0],[5.1,5.5,5.9,6.4,6.9,7.4,7.9],
           [5.6,6.0,6.5,7.0,7.6,8.1,8.6],[6.1,6.5,7.0,7.5,8.1,8.6,9.2],
           [6.4,6.9,7.4,7.9,8.5,9.1,9.7],[6.7,7.2,7.7,8.3,8.9,9.5,10.2],
           [7.0,7.5,8.0,8.6,9.3,9.9,10.5],[7.2,7.7,8.3,8.9,9.6,10.2,10.9],
           [7.5,8.0,8.5,9.2,9.9,10.5,11.2],[7.7,8.2,8.7,9.4,10.1,10.8,11.5],
           [7.8,8.4,9.0,9.6,10.4,11.1,11.8],[8.0,8.6,9.2,9.9,10.6,11.4,12.1],
           [8.2,8.8,9.4,10.1,10.9,11.6,12.4],[8.4,9.0,9.6,10.3,11.1,11.9,12.7],
           [8.5,9.1,9.8,10.5,11.3,12.1,12.9],[8.7,9.3,10.0,10.7,11.6,12.4,13.2],
           [8.9,9.5,10.1,10.9,11.8,12.6,13.5],[9.0,9.7,10.3,11.1,12.0,12.9,13.7],
           [9.2,9.8,10.5,11.3,12.2,13.1,14.0],[9.3,10.0,10.7,11.5,12.5,13.3,14.3],
           [9.5,10.2,10.9,11.8,12.7,13.6,14.5],[9.7,10.3,11.1,12.0,12.9,13.8,14.8],
           [9.8,10.5,11.3,12.2,13.1,14.1,15.1],[10.0,10.7,11.4,12.4,13.3,14.3,15.3],
           [10.1,10.8,11.6,12.5,13.6,14.6,15.6],[10.2,11.0,11.8,12.7,13.8,14.8,15.9],
           [10.4,11.1,12.0,12.9,14.0,15.0,16.1],[10.5,11.3,12.1,13.1,14.2,15.2,16.4],
           [10.7,11.4,12.3,13.3,14.4,15.5,16.6],[10.8,11.6,12.4,13.5,14.6,15.7,16.9],
           [10.9,11.7,12.6,13.7,14.8,15.9,17.1],[11.1,11.9,12.8,13.8,15.0,16.1,17.3],
           [11.2,12.0,12.9,14.0,15.2,16.3,17.6],[11.3,12.2,13.1,14.2,15.4,16.6,17.8],
           [11.4,12.3,13.2,14.3,15.6,16.8,18.0],[11.6,12.4,13.4,14.5,15.8,17.0,18.3],
           [11.7,12.6,13.5,14.7,15.9,17.2,18.5],[11.8,12.7,13.7,14.8,16.1,17.4,18.7],
           [11.9,12.8,13.8,15.0,16.3,17.6,19.0],[12.1,13.0,14.0,15.2,16.5,17.8,19.2],
           [12.2,13.1,14.1,15.3,16.7,18.0,19.4],[12.3,13.2,14.3,15.5,16.9,18.2,19.7],
           [12.4,13.4,14.4,15.7,17.1,18.4,19.9],[12.5,13.5,14.6,15.8,17.3,18.6,20.1],
           [12.7,13.6,14.7,16.0,17.4,18.9,20.4],[12.8,13.8,14.9,16.2,17.6,19.1,20.6],
           [12.9,13.9,15.0,16.3,17.8,19.3,20.9],[13.0,14.0,15.2,16.5,18.0,19.5,21.1],
           [13.1,14.2,15.3,16.7,18.2,19.7,21.3],[13.3,14.3,15.4,16.8,18.4,19.9,21.6],
           [13.4,14.4,15.6,17.0,18.6,20.1,21.8],[13.5,14.6,15.7,17.2,18.8,20.3,22.1],
           [13.6,14.7,15.9,17.3,19.0,20.6,22.3],[13.7,14.8,16.0,17.5,19.2,20.8,22.5],
           [13.8,14.9,16.2,17.7,19.3,21.0,22.8],[13.9,15.1,16.3,17.8,19.5,21.2,23.0],
           [14.1,15.2,16.5,18.0,19.7,21.4,23.3],[14.2,15.3,16.6,18.2,19.9,21.6,23.5],
           [14.3,15.5,16.7,18.3,20.1,21.9,23.8]]
    wgirls_m = [[2.4,2.7,2.9,3.2,3.6,3.9,4.2],[3.2,3.5,3.8,4.2,4.6,5.0,5.4],
            [4.0,4.3,4.7,5.1,5.6,6.0,6.5],[4.6,5.0,5.4,5.8,6.4,6.9,7.4],
            [5.1,5.5,5.9,6.4,7.0,7.5,8.1],[5.5,5.9,6.4,6.9,7.5,8.1,8.7],
            [5.8,6.2,6.7,7.3,7.9,8.5,9.2],[6.1,6.5,7.0,7.6,8.3,8.9,9.6],
            [6.3,6.8,7.3,7.9,8.6,9.3,10.0],[6.6,7.0,7.6,8.2,8.9,9.6,10.4],
            [6.8,7.3,7.8,8.5,9.2,9.9,10.7],[7.0,7.5,8.0,8.7,9.5,10.2,11.0],
            [7.1,7.7,8.2,8.9,9.7,10.5,11.3],[7.3,7.9,8.4,9.2,10.0,10.8,11.6],
            [7.5,8.0,8.6,9.4,10.2,11.0,11.9],[7.7,8.2,8.8,9.6,10.4,11.3,12.2],
            [7.8,8.4,9.0,9.8,10.7,11.5,12.5],[8.0,8.6,9.2,10.0,10.9,11.8,12.7],
            [8.2,8.8,9.4,10.2,11.1,12.0,13.0],[8.3,8.9,9.6,10.4,11.4,12.3,13.3],
            [8.5,9.1,9.8,10.6,11.6,12.5,13.5],[8.7,9.3,10.0,10.9,11.8,12.8,13.8],
            [8.8,9.5,10.2,11.1,12.0,13.0,14.1],[9.0,9.7,10.4,11.3,12.3,13.3,14.3],
            [9.2,9.8,10.6,11.5,12.5,13.5,14.6],[9.3,10.0,10.8,11.7,12.7,13.8,14.9],
            [9.5,10.2,10.9,11.9,12.9,14.0,15.2],[9.6,10.4,11.1,12.1,13.2,14.3,15.4],
            [9.8,10.5,11.3,12.3,13.4,14.5,15.7],[10.0,10.7,11.5,12.5,13.6,14.7,16.0],
            [10.1,10.9,11.7,12.7,13.8,15.0,16.2],[10.3,11.0,11.9,12.9,14.1,15.2,16.5],
            [10.4,11.2,12.0,13.1,14.3,15.5,16.8],[10.5,11.3,12.2,13.3,14.5,15.7,17.0],
            [10.7,11.5,12.4,13.5,14.7,15.9,17.3],[10.8,11.6,12.5,13.7,14.9,16.2,17.6],
            [11.0,11.8,12.7,13.9,15.1,16.4,17.8],[11.1,11.9,12.9,14.0,15.3,16.7,18.1],
            [11.2,12.1,13.0,14.2,15.6,16.9,18.4],[11.4,12.2,13.2,14.4,15.8,17.1,18.6],
            [11.5,12.4,13.4,14.6,16.0,17.4,18.9],[11.6,12.5,13.5,14.8,16.2,17.6,19.2],
            [11.8,12.7,13.7,15.0,16.4,17.9,19.5],[11.9,12.8,13.9,15.2,16.6,18.1,19.7],
            [12.0,13.0,14.0,15.3,16.8,18.3,20.0],[12.1,13.1,14.2,15.5,17.0,18.6,20.3],
            [12.3,13.2,14.3,15.7,17.3,18.8,20.6],[12.4,13.4,14.5,15.9,17.5,19.1,20.8],
            [12.5,13.5,14.7,16.1,17.7,19.3,21.1],[12.6,13.7,14.8,16.3,17.9,19.5,21.4],
            [12.8,13.8,15.0,16.4,18.1,19.8,21.7],[12.9,13.9,15.1,16.6,18.3,20.0,22.0],
            [13.0,14.1,15.3,16.8,18.5,20.3,22.2],[13.1,14.2,15.4,17.0,18.7,20.5,22.5],
            [13.2,14.3,15.6,17.2,18.9,20.8,22.8],[13.4,14.5,15.8,17.3,19.1,21.0,23.1],
            [13.5,14.6,15.9,17.5,19.3,21.2,23.3],[13.6,14.8,16.1,17.7,19.6,21.5,23.6],
            [13.7,14.9,16.2,17.9,19.8,21.7,23.9],[13.8,15.0,16.4,18.0,20.0,21.9,24.4],
            [14.0,15.2,16.5,18.2,20.2,22.2,24.4]]
    hgirls_w = [[45.6,46.7,47.8,49.1,50.4,51.5,52.7],
            [46.8,47.9,49.0,50.3,51.6,52.7,53.9],
            [47.9,49.0,50.2,51.5,52.7,53.9,55.1],
            [48.8,50.0,51.1,52.5,53.7,54.9,56.1],
            [49.7,50.8,52.0,53.4,54.6,55.8,57.0],
            [50.5,51.7,52.9,54.2,55.5,56.7,57.9],
            [51.3,52.5,53.7,55.1,56.4,57.6,58.8],
            [52.1,53.2,54.4,55.8,57.1,58.4,59.6],
            [52.8,54.0,55.3,56.6,58.0,59.2,60.7],
            [53.4,54.7,55.9,57.3,58.7,59.9,61.1],
            [54.1,55.3,56.5,57.9,59.3,60.5,61.8],
            [54.7,55.9,57.1,58.6,59.9,61.2,62.5],
            [55.3,56.5,57.8,59.2,60.6,61.9,63.1]]
    hboys_w = [[46.3,47.4,48.6,49.9,51.1,52.3,53.4],
           [47.5,48.6,49.8,51.1,52.3,53.5,54.7],
           [48.8,49.8,51.0,52.3,53.6,54.7,55.9],
           [49.8,50.9,52.0,53.4,54.6,55.8,57.0],
           [50.7,51.9,53.0,54.4,55.6,56.8,58.0],
           [51.7,52.8,54.0,55.3,56.6,57.8,59.0],
           [52.5,53.7,54.9,56.2,57.5,58.7,59.9],
           [53.4,54.5,55.7,57.1,58.4,59.6,60.8],
           [54.1,55.3,56.5,57.9,59.2,60.4,61.6],
           [54.9,56.0,57.3,58.7,60.0,61.2,62.4],
           [55.6,59.8,58.0,59.4,60.7,61.9,63.2],
           [56.3,57.4,58.7,60.1,61.4,62.6,63.9],
           [56.9,58.1,59.3,60.8,62.1,63.3,64.6]]
    hboys_m = [[46.3,47.4,48.6,49.9,51.1,52.3,53.4],[51.1,52.3,53.4,54.7,56.1,57.2,58.4],
           [54.7,55.9,57.1,58.4,59.8,61.1,62.2],[57.6,58.8,60.1,61.4,62.8,64.1,65.3],
           [60.0,61.2,62.5,63.9,65.3,66.5,67.8],[61.9,63.2,64.5,65.9,67.3,68.6,69.9],
           [63.6,64.9,66.2,67.6,69.1,70.4,71.6],[65.1,66.4,67.7,69.2,70.6,71.9,73.2],
           [66.5,67.8,69.1,70.6,72.1,73.4,74.7],[67.7,69.1,70.5,72.0,73.5,74.8,76.2],
           [69.0,70.4,71.8,73.3,74.8,76.2,77.6],[70.2,71.5,73.0,74.5,76.1,77.5,78.9],
           [71.3,72.7,74.1,75.7,77.3,78.8,80.2],[72.4,73.8,75.3,76.9,78.6,80.0,81.5],
           [73.4,74.9,76.4,78.0,79.7,81.2,82.7],[74.4,75.9,77.4,79.1,80.8,82.4,83.9],
           [75.4,76.9,78.5,80.2,82.0,83.5,85.1],[76.3,77.9,79.5,81.2,83.0,84.6,86.2],
           [77.2,78.8,80.5,82.3,84.1,85.7,87.3],[78.1,79.7,81.4,83.2,85.1,86.8,88.4],
           [78.9,80.6,82.3,84.2,86.1,87.8,89.5],[79.7,81.5,83.2,85.1,87.1,88.8,90.5],
           [80.5,82.3,84.1,86.0,88.0,89.8,91.6],[81.3,83.1,84.9,86.9,89.0,90.8,92.6],
           [81.4,83.2,85.0,87.1,89.2,91.0,92.9],[82.1,84.0,85.9,88.0,90.1,92.0,93.8],
           [82.8,84.7,86.7,88.8,90.9,92.9,94.8],[83.5,85.5,87.4,89.6,91.8,93.8,95.7],
           [84.2,86.2,88.2,90.4,92.7,94.7,96.6],[84.9,86.9,89.0,91.2,93.5,95.5,97.5],
           [85.5,87.6,89.7,91.9,94.3,96.3,98.3],[86.2,88.2,90.3,92.7,95.0,97.1,99.2],
           [86.8,88.9,91.0,93.4,95.7,97.9,100.0],[87.4,89.5,91.7,94.1,96.5,98.7,100.8],
           [88.0,90.2,92.4,94.8,97.2,99.4,101.5],[88.5,90.7,93.0,95.4,97.9,100.1,102.3],
           [89.1,91.3,93.6,96.1,98.6,100.8,103.1],[89.7,91.9,94.2,96.7,99.3,101.5,103.8],
           [90.2,92.5,94.8,97.4,99.9,102.2,104.5],[90.8,93.1,95.4,98.0,100.6,102.9,105.2],
           [91.3,93.7,96.0,98.6,101.3,103.6,105.9],[91.9,94.2,96.6,99.2,101.9,104.3,106.6],
           [92.4,94.8,97.2,99.9,102.5,104.9,107.3],[92.9,95.3,97.7,100.4,103.1,105.6,108.0],
           [93.4,95.9,98.3,101.0,103.8,106.2,108.6],[93.9,96.4,98.9,101.6,104.4,106.8,109.3],
           [94.4,96.9,99.4,102.2,105.0,107.5,109.9],[94.9,97.4,100.2,102.8,105.6,108.1,110.6],
           [95.4,98.0,100.5,103.3,106.2,108.7,111.2],[95.9,98.5,101.8,103.9,106.7,109.3,111.8],
           [96.4,99.0,101.6,104.4,107.3,109.9,112.5],[96.9,99.5,102.1,105.0,107.9,110.5,113.1],
           [97.4,100.0,102.6,105.6,108.5,111.1,113.7],[97.9,100.5,103.1,106.1,109.1,111.7,114.3],
           [98.4,101.0,103.7,106.7,109.6,112.3,115.0],[98.8,101.5,104.2,107.2,110.2,112.9,115.6],
           [99.3,102.0,104.7,107.8,110.8,113.5,116.2],[99.8,102.5,105.3,108.3,111.4,114.1,116.8],
           [100.3,103.0,105.8,108.9,111.9,114.7,117.4],[100.8,103.5,106.3,109.4,112.5,115.3,118.1],
           [101.2,104.0,106.8,110.0,113.1,115.9,118.7]]
    hgirls_m = [[45.6,46.7,47.8,49.1,50.4,51.5,52.7],[50.0,51.2,52.4,53.7,55.0,56.2,57.4],
            [53.2,54.4,55.7,57.1,58.4,59.6,60.9],[55.8,57.1,58.4,59.8,61.2,62.5,63.8],
            [58.0,59.3,60.6,62.1,63.5,64.8,66.2],[59.9,61.2,62.5,64.0,65.5,66.9,68.2],
            [61.5,62.8,64.2,65.7,67.3,68.7,70.0],[62.9,64.3,65.7,67.3,68.8,70.3,71.6],
            [64.3,65.7,67.2,68.7,70.4,71.8,73.2],[65.6,67.0,68.5,70.1,71.8,73.2,74.7],
            [66.8,68.3,69.8,71.5,73.1,74.6,76.1],[68.0,69.5,71.1,72.8,74.5,76.0,77.5],
            [69.2,70.7,72.3,74.0,75.7,77.3,78.9],[70.3,71.8,73.4,75.2,77.0,78.6,80.2],
            [71.3,73.0,74.6,76.4,78.2,79.8,81.4],[72.4,74.0,75.7,77.5,79.4,81.0,82.7],
            [73.3,75.0,76.7,78.6,80.5,82.2,83.9],[74.3,76.0,77.7,79.7,81.6,83.3,85.0],
            [75.2,76.9,78.7,80.7,82.6,84.4,86.2],[76.2,77.9,79.7,81.7,83.7,85.5,87.3],
            [77.0,78.8,80.7,82.7,84.7,86.6,88.4],[77.9,79.7,81.6,83.7,85.7,87.6,89.4],
            [78.7,80.6,82.5,84.6,86.7,88.6,90.5],[79.6,81.4,83.4,85.5,87.6,89.6,91.5],
            [79.6,81.6,83.5,85.7,87.9,89.8,91.8],[80.4,82.4,84.4,86.6,88.8,90.8,92.8],
            [81.2,83.2,85.2,87.4,89.7,91.7,93.7],[81.9,83.9,86.0,88.3,90.5,92.6,94.6],
            [82.6,84.7,86.8,89.1,91.4,93.5,95.6],[83.4,85.4,87.5,89.9,92.2,94.3,96.4],
            [84.0,86.2,88.3,90.7,93.1,95.2,97.3],[84.7,86.9,89.0,91.4,93.9,96.0,98.2],
            [85.4,87.5,89.7,92.2,94.6,96.8,99.0],[86.0,88.2,90.5,92.9,95.4,97.6,99.8],
            [86.7,88.9,91.2,93.6,96.2,98.4,100.6],[87.3,89.6,91.8,94.4,96.9,99.2,101.4],
            [87.9,90.2,92.5,95.1,97.7,100.0,102.2],[88.5,90.8,93.1,95.7,98.3,100.7,103.0],
            [89.1,91.4,93.8,96.4,99.0,101.4,103.7],[89.7,92.0,94.4,97.1,99.7,102.1,104.5],
            [90.3,92.6,95.1,97.7,100.4,102.9,105.2],[90.8,93.3,95.7,98.4,101.1,103.6,106.0],
            [91.4,93.8,96.3,99.0,101.8,104.3,106.7],[92.0,94.4,96.9,99.7,102.5,104.9,107,4],
            [92.5,95.0,97.5,100.3,103.1,105.7,108.1],[93.0,95.5,98.1,100.9,103.7,106.3,108.8],
            [93.6,96.1,98.7,101.5,104.4,107.0,109.5],[94.1,96.7,99.3,102.1,105.0,107.6,110.2],
            [94.6,97.2,99.8,102.7,105.6,108.3,110.8],[95.1,97.7,100.4,103.3,106.2,108.9,111.5],
            [95.7,98.3,101.0,103.9,106.9,109.5,112.1],[96.2,98.9,101.5,104.5,107.5,110.2,112.8],
            [96.7,99.3,102.0,105.0,108.1,110.8,113.4],[97.2,99.9,102.6,105.6,108.7,111.4,114.1],
            [97.6,100.4,103.1,106.2,109.2,112.0,114.7],[98.1,100.9,103.7,106.7,109.8,112.6,115.3],
            [98.6,101.4,104.2,107.3,110.4,113.2,116.0],[99.1,101.9,104.7,107.8,111.0,113.8,116.6],
            [99.6,102.4,105.2,108.4,111.6,114.4,117.8],[100.0,102.9,105.7,108.9,112.1,115.0,117.8],
            [100.5,103.4,106.3,109.4,112.7,115.6,118.4]]
    bmiboys_w = [[11.3,11.9,12.6,13.4,14.8,15.2,16.1],[11.0,11.7,12.5,13.3,14.2,15.1,15.9],
             [11.3,12.0,12.8,13.6,14.5,15.3,16.2],[11.9,12.6,13.4,14.2,15.1,16.0,16.8],
             [12.4,13.1,13.9,14.8,15.7,16.6,17.4],[12.8,13.6,14.3,15.2,16.2,17.1,18.0],
             [13.2,13.9,14.7,15.6,16.6,17.5,18.4],[13.5,14.2,15.0,15.9,16.9,17.8,18.7],
             [13.7,14.4,15.2,16.2,17.1,18.1,19.0],[13.9,14.6,15.4,16.4,17.4,18.3,19.3],
             [14.1,14.8,15.6,16.5,17.5,18.5,19.4],[14.2,14.9,15.7,16.7,17.7,18.6,19.6],
             [14.3,15.4,16.8,18.4,19.7]]
    bmigirls_w = [[11.2,11.8,12.5,13.3,14.2,15.0,15.9],[10.8,11.6,12.3,13.2,14.1,14.9,15.8],
              [11.1,11.8,12.6,13.5,14.3,15.2,16.0],[11.5,12.3,13.1,14.0,14.9,15.7,16.6],
              [12.0,12.7,13.5,14.4,15.4,16.3,17.2],[12.3,13.1,13.9,14.8,15.8,16.7,17.6],
              [12.6,13.4,14.2,15.1,16.1,17.1,18.0],[12.9,13.6,14.4,15.4,16.4,17.4,18.3],
              [13.1,13.8,14.7,15.6,16.6,17.6,18.6],[13.2,14.0,14.9,15.8,16.8,17.8,18.8],
              [13.4,14.2,15.0,16.0,17.0,18.0,19.0],[13.5,14.3,15.1,16.1,17.2,18.2,19.2],
              [13.6,14.4,15.3,16.2,17.3,18.3,19.3]]
    bmiboys_m = [[11.3,11.9,12.6,13.4,14.3,15.2,16.1],[12.6,13.3,14.1,14.9,15.9,16.7,17.6], 
             [13.8,14.6,15.4,16.3,17.3,18.2,19.2],[14.4,15.2,16.0,16.9,17.9,18.8,19.8],
             [14.7,15.4,16.2,17.2,18.2,19.1,20.1],[14.8,15.6,16.4,17.3,18.3,19.2,20.2],
             [14.9,15.6,16.4,17.3,18.3,19.3,20.3],[14.9,15.6,16.4,17.3,18.3,19.3,20.3],
             [14.9,15.6,16.3,17.3,18.2,19.2,20.2],[14.8,15.5,16.3,17.2,18.1,19.1,20.1],
             [14.7,15.4,16.2,17.0,18.0,18.9,19.9],[14.6,15.3,16.0,16.9,17.9,18.8,19.8],
             [14.5,15.2,15.9,16.8,17.7,18.7,19.6],[14.4,15.1,15.8,16.7,17.6,18.5,19.5],
             [14.3,15.0,15.7,16.6,17.5,18.4,19.3],[14.2,14.9,15.6,16.4,17.4,18.2,19.2],
             [14.2,14.8,15.5,16.3,17.2,18.1,19.1],[14.1,14.7,15.4,16.2,17.1,18.0,18.9],
             [14.0,14.6,15.3,16.1,17.1,17.9,18.8],[13.9,14.6,15.2,16.1,16.9,17.8,18.7],
             [13.9,14.5,15.2,16.0,16.9,17.7,18.6],[13.8,14.4,15.1,15.9,16.8,17.6,18.6],
             [13.8,14.4,15.0,15.8,16.7,17.6,18.5],[13.7,14.3,15.0,15.8,16.7,17.5,18.4],
             [13.3,14.3,14.9,15.7,16.6,17.4,18.3],[13.9,14.5,15.2,16.0,16.9,17.7,18.6],
             [13.8,14.5,15.1,15.9,16.8,17.7,18.6],[13.8,14.4,15.1,15.9,16.8,17.6,18.5],
             [13.8,14.4,15.1,15.9,16.7,17.6,18.5],[13.7,14.4,15.0,15.8,16.7,17.5,18.4],
             [13.7,14.3,15.0,15.8,16.7,17.5,18.4],[13.7,14.3,15.0,15.8,16.6,17.5,18.4],
             [13.6,14.2,14.9,15.7,16.6,17.4,18.3],[13.6,14.2,14.9,15.7,16.6,17.4,18.3],
             [13.5,14.2,14.9,15.7,16.5,17.4,18.2],[13.5,14.1,14.8,15.6,16.5,17.3,18.2],
             [13.5,14.1,14.8,15.6,16.5,17.3,18.2],[13.5,14.1,14.8,15.6,16.4,17.3,18.1],
             [13.4,14.1,14.7,15.5,16.4,17.2,18.1],[13.4,14.0,14.7,15.5,16.4,17.2,18.1],
             [13.4,14.0,14.7,15.5,16.4,17.2,18.1],[13.3,14.0,14.7,15.5,16.3,17.2,18.0],
             [13.3,13.9,14.6,15.4,16.3,17.1,18.0],[13.3,13.9,14.6,15.4,16.3,17.1,18.0],
             [13.3,13.9,14.6,15.4,16.3,17.1,18.0],[13.3,13.9,14.6,15.4,16.3,17.1,18.0],
             [13.2,13.9,14.5,15.4,16.2,17.1,18.0],[13.2,13.8,14.5,15.3,16.2,17.1,18.0],
             [13.2,13.8,14.5,15.3,16.2,17.1,18.0],[13.2,13.8,14.5,15.3,16.2,17.1,18.0],
             [13.2,13.8,14.5,15.3,16.2,17.1,18.0],[13.1,13.8,14.5,15.3,16.2,17.1,18.0],
             [13.1,13.8,14.4,15.3,16.2,17.1,18.0],[13.1,13.7,14.4,15.3,16.2,17.1,18.0],
             [13.1,13.7,14.4,15.3,16.2,17.0,18.0],[13.1,13.7,14.4,15.2,16.2,17.0,18.0],
             [13.1,13.7,14.4,15.2,16.1,17.0,18.0],[13.0,13.7,14.4,15.2,16.1,17.1,18.0],
             [13.0,13.7,14.4,15.2,16.1,17.1,18.0],[13.0,13.7,14.4,15.2,16.1,17.1,18.1],
             [13.0,13.6,14.3,15.2,16.1,17.1,18.1]]
    bmigirls_m = [[11.2,11.8,12.5,13.3,14.2,15.0,15.9],[12.1,12.9,13.6,14.6,15.5,16.4,17.3],
              [13.2,14.0,14.8,15.8,16.8,17.8,18.8],[13.7,14.5,15.4,16.4,17.4,18.4,19.4],
              [14.0,14.8,15.7,16.7,17.7,18.8,19.8],[14.2,15.0,15.8,16.8,17.9,18.9,20.0],
              [14.3,15.1,15.9,16.9,18.0,19.0,20.1],[14.3,15.1,15.9,16.9,18.0,19.0,20.1],
              [14.3,15.0,15.9,16.8,17.9,18.9,20.0],[14.2,15.0,15.8,16.7,17.8,18.8,19.9],
              [14.1,14.9,15.7,16.6,17.7,18.7,19.7],[14.0,14.8,15.5,16.5,17.5,18.5,19.6],
              [13.9,14.6,15.4,16.4,17.4,18.4,19.4],[13.8,14.5,15.3,16.2,17.2,18.2,19.2],
              [13.7,14.4,15.2,16.1,17.1,18.1,19.1],[13.7,14.3,15.1,16.0,17.0,17.9,19.0],
              [13.6,14.3,15.0,15.9,16.9,17.8,18.8],[13.5,14.2,14.9,15.8,16.8,17.7,18.7],
              [13.4,14.1,14.8,15.7,16.7,17.6,18.6],[13.4,14.1,14.8,15.7,16.6,17.5,18.5],
              [13.3,14.0,14.7,15.6,16.5,17.5,18.5],[13.3,14.0,14.7,15.5,16.5,17.4,18.4],
              [13.3,13.9,14.6,15.5,16.4,17.3,18.3],[13.2,13.9,14.6,15.4,16.4,17.3,18.3],
              [13.2,13.9,14.6,15.4,16.3,17.3,18.2],[13.4,14.1,14.8,15.7,16.6,17.5,18.5],
              [13.4,14.1,14.8,15.6,16.6,17.5,18.5],[13.4,14.0,14.8,15.6,16.5,17.4,18.4],
              [13.4,14.0,14.7,15.6,16.5,17.4,18.4],[13.4,14.0,14.7,15.6,16.5,17.4,18.4],
              [13.3,14.0,14.7,15.5,16.5,17.4,18.3],[13.3,14.0,14.7,15.5,16.4,17.3,18.3],
              [13.3,13.9,14.6,15.5,16.4,17.3,18.3],[13.3,13.9,14.6,15.5,16.4,17.3,18.3],
              [13.2,13.9,14.6,15.4,16.4,17.3,18.2],[13.2,13.9,14.6,15.4,16.3,17.3,18.2],
              [13.2,13.8,14.5,15.4,16.3,17.2,18.2],[13.2,13.8,14.5,15.4,16.3,17.2,18.2],
              [13.2,13.8,14.5,15.4,16.3,17.2,18.2],[13.1,13.8,14.5,15.3,16.3,17.2,18.2],
              [13.1,13.8,14.5,15.3,16.3,17.2,18.2],[13.1,13.7,14.5,15.3,16.3,17.2,18.2],
              [13.1,13.7,14.4,15.3,16.3,17.2,18.2],[13.0,13.7,14.4,15.3,16.3,17.2,18.2],
              [13.0,13.7,14.4,15.3,16.3,17.2,18.2],[13.0,13.7,14.4,15.3,16.3,17.2,18.3],
              [13.0,13.7,14.4,15.3,16.3,17.2,18.3],[13.0,13.6,14.4,15.3,16.3,17.2,18.3],
              [12.9,13.6,14.4,15.3,16.3,17.2,18.3],[12.9,13.6,14.4,15.3,16.3,17.2,18.3],
              [12.9,13.6,14.3,15.3,16.3,17.3,18.3],[12.9,13.6,14.3,15.3,16.3,17.3,18.4],
              [12.9,13.6,14.3,15.3,16.3,17.3,18.4],[12.9,13.6,14.3,15.3,16.3,17.3,18.4],
              [12.9,13.6,14.3,15.3,16.3,17.3,18.4],[12.9,13.5,14.3,15.3,16.3,17.3,18.4],
              [12.8,13.5,14.3,15.3,16.3,17.3,18.5],[12.8,13.5,14.3,15.3,16.3,17.4,18.5],
              [12.8,13.5,14.3,15.3,16.3,17.4,18.5],[12.8,13.5,14.3,15.3,16.3,17.4,18.5],
              [12.8,13.5,14.3,15.3,16.3,17.4,18.6]]
    percentiles = ["menor a P3","P3 y P10","P10 y P25","P25 y P50","P50 y P75","P75 y P90","P90 y P97","mayor a P97"]
    percentiles_exacto = ["P3","P10","P25","P50","P75","P90","P97"]
    usuario = fields.Many2One('gnuhealth.patient','Apellido y Nombre', required=True)
    has_newborn_record = fields.Function(
        fields.Boolean('Birth certificate', 
        help="Has newborn record on system?"),
        'get_newborn_record')
    dni = fields.Function(
        fields.Char('DNI'),
        'get_dni', searcher='search_dni')
    w_perc = fields.Char ('Percentil Peso')
    h_perc = fields.Char ('Percentil Talla')
    bmi_perc = fields.Char ('Percentil IMC')
    fecha_control = fields.Date('Fecha de Control', required=True)
    fecha_entrega  = fields.Date('Fecha de Entrega', required=True)    

    def get_dni(self, usuario):
        if self.usuario:
           return self.usuario.name.ref
    
    @classmethod
    def search_dni(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('usuario.name.ref', clause[1], value))
        return res

    fn = fields.Function(fields.Date('Fecha Nac.'),'get_fn')

    def get_fn(self, usuario):
        if self.usuario.name.dob:
           return self.usuario.name.dob  

    edad = fields.Function(fields.Float('Edad', help="Edad a la fecha de Control"),'get_patient_age')

    @fields.depends('usuario', 'fecha_control')
    def on_change_with_edad(self, name=None):
        return self.get_patient_age(self.usuario)
    
    def get_patient_age(self, usuario):
        fecha_cont = self.fecha_control
        if self.usuario and self.usuario.name.dob:
            nac = self.usuario.name.dob
            delta = relativedelta(fecha_cont, nac)
            edad = float(delta.years) + float(delta.months/12.0) + float(delta.days/365.0)
        else:
            edad = 0.0
        return float(edad)
    
    peso_newborn = fields.Float('Peso al Nacer')    
    sem_nacimiento = fields.Integer('Semana de Nac.')
        
    @fields.depends('usuario')
    def on_change_usuario (self):
        pool = Pool()
        Newborn = pool.get('gnuhealth.newborn')
        if self.usuario:
            newborn = Newborn.search([('patient','=',self.usuario.id)])
            if newborn:
                self.peso_newborn = float(newborn[0].weight/1000.0)
                self.sem_nacimiento = int(newborn[0].born_week)
            else:
                self.raise_user_error('El menor debe estar ingresado en "Neonato" con sus datos al nacimiento')
        else:
            self.peso_newborn = 0.0
            self.sem_nacimiento = 0

    peso = fields.Float('Peso [Kg]', required=True)
    talla = fields.Float('Talla [cm]', required=True)
    talla_m = fields.Function(fields.Float('Talla[m]'), 'get_tallam') 
    
    def get_tallam(self, name):
        talla_m = (self.talla)/100
        return talla_m
    
    
#    @staticmethod
#    def default_talla():
#      return 45.0
    imc = fields.Float('IMC')
    
    @fields.depends('peso','talla')
    def on_change_with_imc(self):
        imc = 0
        if self.peso and self.talla:
            imc = float(self.peso) / pow((float(self.talla)/100),2)
            if imc > 50 or imc <1:
                self.raise_user_error("Verificar valores de Peso (en Kilogramos) y Talla (en centímetros)")
        return float(imc)

    diag_nut = fields.Selection([
        (None, ''),
        ('N', 'Normal'),
        ('RBP', 'Riesgo Bajo Peso'),
        ('BP', 'Bajo Peso'),
        ('SP', 'Sobrepeso'),
        ('OB', 'Obesidad'),
        ('TB', 'Talla Baja'),
        ('CL', 'Crecimiento Lento'),
        ('REC', 'Recuperacion'),
        ], 'Diag. Nutricional',
        required=True, sort=False)
    diag_normal = fields.Function(fields.Char('Diag N'), 'get_diag_normal')
    
    def get_diag_normal(self, name):
        if self.diag_nut == 'N':
            diag_normal = 'X'
        else:
            diag_normal=' '
        return diag_normal

    diag_rbp = fields.Function(fields.Char('Diag RBP'), 'get_diag_rbp')
    
    def get_diag_rbp(self, name):
        if self.diag_nut == 'RBP':
            diag_rbp = 'X'
        else:
            diag_rbp=' '
        return diag_rbp
    
    diag_bp = fields.Function(fields.Char('Diag BP'), 'get_diag_bp')
    
    def get_diag_bp(self, name):
        if self.diag_nut == 'BP':
            diag_bp = 'X'
        else:
            diag_bp=' '
        return diag_bp
    
    diag_sp = fields.Function(fields.Char('Diag SP'), 'get_diag_sp')
    
    def get_diag_sp(self, name):
        if self.diag_nut == 'SP':
            diag_sp = 'X'
        else:
            diag_sp=' '
        return diag_sp

    diag_ob = fields.Function(fields.Char('Diag OB'), 'get_diag_ob')
    
    def get_diag_ob(self, name):
        if self.diag_nut == 'OB':
            diag_ob = 'X'
        else:
            diag_ob=' '
        return diag_ob
    
    diag_tb = fields.Function(fields.Char('Diag TB'), 'get_diag_tb')
    
    def get_diag_tb(self, name):
        if self.diag_nut == 'TB':
            diag_tb = 'X'
        else:
            diag_tb=' '
        return diag_tb
    
    diag_cl = fields.Function(fields.Char('Diag CL'), 'get_diag_cl')
    
    def get_diag_cl(self, name):
        if self.diag_nut == 'CL':
            diag_cl = 'X'
        else:
            diag_cl=' '
        return diag_cl
    
    diag_rec = fields.Function(fields.Char('Diag REC'), 'get_diag_rec')
    
    def get_diag_rec(self, name):
        if self.diag_nut == 'REC':
            diag_rec = 'X'
        else:
            diag_rec=' '
        return diag_rec
    
    extra = fields.Integer('Extra Progr.')
    cantidad = fields.Integer('Cantidad [Kg]', required=True)
    observaciones = fields.Char('Observaciones')
    
    # PARA CALCULAR EL PERCENTIL DEL PESO    
    @fields.depends(
            'peso','fecha_control','usuario',
            'wboys_m','wboys_w','wgirls_m','wgirls_w',
            'percentiles','sem_nacimiento'
            )
    def on_change_peso(self):
        edad_w = 0
        edad_m = 0
        correccion = 0
        if self.usuario and self.usuario.name.dob and self.sem_nacimiento:
            if self.sem_nacimiento <37 or self.sem_nacimiento > 40:
                correccion = 37- int(self.sem_nacimiento)
            else:
                correccion = 0
            delta = relativedelta(self.fecha_control, self.usuario.name.dob) 
            edad_w = int(delta.years*365.2425 + delta.months*30.5 + delta.days)//7-correccion
            edad_m = int(delta.years*12 + delta.months)- (correccion//4)  
        if self.usuario and edad_w < 0 and self.peso and edad_w:
            self.w_perc = "Antes de término -> Sin rango establecido"
        elif self.usuario and self.usuario.name.gender == "m" and edad_w >=0 and edad_w < 13 and self.peso:
            if self.peso in self.wboys_w[edad_w]:
                self.w_perc = self.percentiles_exacto[self.wboys_w[edad_w].index(self.peso)]
            else:
                aux = self.wboys_w[edad_w]+ [float(self.peso)]
                aux.sort()
                self.w_perc = self.percentiles[aux.index(self.peso)]
        elif self. usuario and self.usuario.name.gender == "m" and edad_w >= 13 and edad_m and edad_m < 60 and self.peso:
            if self.peso in self.wboys_m[edad_m]:
                self.w_perc = self.percentiles_exacto[self.wboys_m[edad_m].index(self.peso)]
            else:
                aux = self.wboys_m[edad_m]+ [float(self.peso)]
                aux.sort()
                self.w_perc = self.percentiles[aux.index(self.peso)]
        elif self. usuario and self.usuario.name.gender =="m" and edad_m >=60 and self.peso and edad_m:
            w_perc = "Edad fuera del rango de control de percentiles"
        elif self.usuario and self.usuario.name.gender == "f" and edad_w >=0 and edad_w < 13 and self.peso:
            if self.peso in self.wgirls_w[edad_w]:
                self.w_perc = self.percentiles_exacto[self.wgirls_w[edad_w].index(self.peso)]
            else:
                aux = self.wgirls_w[edad_w]+ [float(self.peso)]
                aux.sort()
                self.w_perc = self.percentiles[aux.index(self.peso)]
        elif self.usuario and self.usuario.name.gender == "f" and edad_w >= 13 and edad_m and edad_m < 60 and self.peso:
            if self.peso in self.wgirls_m[edad_m]:
                self.w_perc = self.percentiles_exacto[self.wgirls_m[edad_m].index(self.peso)]
            else:
                aux = self.wgirls_m[edad_m]+ [float(self.peso)]
                aux.sort()
                self.w_perc = self.percentiles[aux.index(self.peso)]
        elif self.usuario and self.usuario.name.gender =="f" and edad_m >=60 and self.peso and edad_m:
            self.w_perc = "Edad fuera del rango de control de percentiles"

    @fields.depends('peso', 'w_perc','diag_nut')
    def on_change_with_diag_nut(self):
        diag_nut = self.diag_nut
        if self.diag_nut == None:
            if self.w_perc == 'P3' or self.w_perc == 'menor a P3':
                diag_nut = 'BP'
            elif self.w_perc == 'P3 y P10':
                diag_nut = 'RBP'
            elif self.w_perc == 'P10' or  self.w_perc == 'P10 y P25' or self.w_perc == 'P25'\
                 or self.w_perc == 'P25 y P50' or self.w_perc == 'P50' or self.w_perc == 'P50 y P75'\
                 or self.w_perc == 'P75' or self.w_perc == 'P75 y P90':
                diag_nut = 'N'
                print(self.w_perc)
            elif self.w_perc == 'P90' or self.w_perc == 'P90 y P97':
                diag_nut = 'SP'
            else:
                diag_nut = 'OB'
        return diag_nut

    @fields.depends('peso')
    def on_change_with_cantidad(self):   
        if self.usuario.age_float >=1:
            if  self.w_perc=='menor a P3' or self.w_perc =='P3' or self.w_perc =='P3 y P10':
                cantidad = 3
            else:
                cantidad = 1
        else:
            cantidad = 2
        return cantidad

    # PARA CALCULAR EL PERCENTIL DE LA ALTURA
    @fields.depends('talla','fecha_control','usuario','hboys_m','hboys_w','hgirls_m','hgirls_w','percentiles','sem_nacimiento')
    def on_change_with_h_perc(self):
        edad_w = 0
        edad_m = 0
        correccion = 0
        if self.usuario and self.usuario.name.dob and self.sem_nacimiento:
            if self.sem_nacimiento <37 or self.sem_nacimiento > 40:
                correccion = 37- int(self.sem_nacimiento)
            else:
                correccion = 0
            delta = relativedelta(self.fecha_control, self.usuario.name.dob) 
            edad_w = int(delta.years*365.2425 + delta.months*30.5 + delta.days)//7-correccion
            edad_m = int(delta.years*12 + delta.months)- (correccion//4)
        if self.usuario and edad_w < 0 and self.talla and edad_w:
            h_perc = "Antes de término -> Sin rango establecido"
            return h_perc
        elif self.usuario and self.usuario.name.gender == "m" and edad_w >=0 and edad_w <13 and self.talla:
            if self.talla in self.hboys_w[edad_w]:
                h_perc = self.percentiles_exacto[self.hboys_w[edad_w].index(self.talla)]
                return h_perc
            else:
                aux = self.hboys_w[edad_w]+ [float(self.talla)]
                aux.sort()
                h_perc = self.percentiles[aux.index(self.talla)]
                return h_perc
        elif self. usuario and self.usuario.name.gender == "m" and edad_w >= 13 and edad_m and edad_m < 60 and self.talla:
            if self.talla in self.hboys_m[edad_m]:
                h_perc = self.percentiles_exacto[self.hboys_m[edad_m].index(self.talla)]
                return h_perc
            else:
                aux = self.hboys_m[edad_m]+ [float(self.talla)]
                aux.sort()
                h_perc = self.percentiles[aux.index(self.talla)]
                return h_perc
        elif self. usuario and self.usuario.name.gender =="m" and edad_m >=60 and self.talla and edad_m:
            h_perc = "Edad fuera del rango de control de percentiles"
            return h_perc
        elif self.usuario and self.usuario.name.gender == "f" and edad_w >=0 and edad_w < 13 and self.talla:
            if self.talla in self.hgirls_w[edad_w]:
                h_perc = self.percentiles_exacto[self.hgirls_w[edad_w].index(self.talla)]
                return h_perc
            else:
                aux = self.hgirls_w[edad_w]+ [float(self.talla)]
                aux.sort()
                h_perc = self.percentiles[aux.index(self.talla)]
                return h_perc
        elif self.usuario and self.usuario.name.gender == "f" and edad_w >= 13 and edad_m and edad_m < 60 and self.talla:
            if self.talla in self.hgirls_m[edad_m]:
                h_perc = self.percentiles_exacto[self.hgirls_m[edad_m].index(self.talla)]
                return h_perc
            else:
                aux = self.hgirls_m[edad_m]+ [float(self.talla)]
                aux.sort()
                h_perc = self.percentiles[aux.index(self.talla)]
                return h_perc
        elif self.usuario and self.usuario.name.gender =="f" and edad_m >=60 and self.talla and edad_m:
            h_perc = "Edad fuera del rango de control de percentiles"
            return h_perc
        
    # PARA CALCULAR EL PERCENTIL DEL ÍNDICE DE MASA CORPORAL
    @fields.depends('peso','talla','fecha_control','usuario','bmiboys_m','bmiboys_w','bmigirls_m','bmigirls_w','percentiles','sem_nacimiento')
    def on_change_with_bmi_perc(self):
        edad_w = 0
        edad_m = 0
        bmi = 0.0
        correccion = 0
        if self.usuario and self.usuario.name.dob and self.sem_nacimiento:
            if self.sem_nacimiento <37 or self.sem_nacimiento > 40:
                correccion = 37- int(self.sem_nacimiento)
            else:
                correccion = 0
            delta = relativedelta(self.fecha_control, self.usuario.name.dob) 
            edad_w = int(delta.years*365.2425 + delta.months*30.5 + delta.days)//7-correccion
            edad_m = int(delta.years*12 + delta.months)- (correccion//4)
        if self.peso and self.talla:
            bmi = float(self.peso) / pow((float(self.talla)/100),2)            
        if self.usuario and edad_w < 0 and self.peso and self.talla and edad_w:
            bmi_perc = "Antes de término -> Sin rango establecido"
            return bmi_perc
        elif self.usuario and self.usuario.name.gender == "m" and edad_w >=0 and edad_w < 13 and self.peso and self.talla:
            if bmi in self.bmiboys_w[edad_w]:
                bmi_perc = self.percentiles_exacto[self.bmiboys_w[edad_w].index(bmi)]
                return bmi_perc
            else:
                aux = self.bmiboys_w[edad_w]+ [bmi]
                aux.sort()
                bmi_perc = self.percentiles[aux.index(bmi)]
                return bmi_perc
        elif self. usuario and self.usuario.name.gender == "m" and edad_w >= 13 and edad_m and edad_m < 60 and self.peso and self.talla:
            if bmi in self.bmiboys_m[edad_m]:
                bmi_perc = self.percentiles_exacto[self.bmiboys_m[edad_m].index(bmi)]
                return bmi_perc
            else:
                aux = self.bmiboys_m[edad_m]+ [bmi]
                aux.sort()
                bmi_perc = self.percentiles[aux.index(bmi)]
                return bmi_perc
        elif self. usuario and self.usuario.name.gender =="m" and edad_m >=60 and self.peso and self.talla and edad_m:
            bmi_perc = "Edad fuera del rango de control de percentiles"
            return bmi_perc
        elif self.usuario and self.usuario.name.gender == "f" and edad_w >=0 and edad_w < 13 and self.peso and self.talla:
            if bmi in self.bmigirls_w[edad_w]:
                bmi_perc = self.percentiles_exacto[self.bmigirls_w[edad_w].index(bmi)]
                return bmi_perc
            else:
                aux = self.bmigirls_w[edad_w]+ [bmi]
                aux.sort()
                bmi_perc = self.percentiles[aux.index(bmi)]
                return bmi_perc
        elif self.usuario and self.usuario.name.gender == "f" and edad_w >= 13 and edad_m and edad_m < 60 and self.peso and self.talla:
            if bmi in self.bmigirls_m[edad_m]:
                bmi_perc = self.percentiles_exacto[self.bmigirls_m[edad_m].index(bmi)]
                return bmi_perc
            else:
                aux = self.bmigirls_m[edad_m]+ [bmi]
                aux.sort()
                bmi_perc = self.percentiles[aux.index(bmi)]
                return bmi_perc
        elif self.usuario and self.usuario.name.gender =="f" and edad_m >=60 and self.peso and edad_m:
            bmi_perc = "Edad fuera del rango de control de percentiles"
            return bmi_perc

    @fields.depends('usuario')
    def on_change_with_has_newborn_record(self):
        pool = Pool()
        Newborn = pool.get('gnuhealth.newborn')
        if self.usuario and Newborn.search(['patient','=',self.usuario.id]):
            return True
        return False
    
    def get_newborn_record(self, name):
        self.on_change_with_has_newborn_record()
    
    
