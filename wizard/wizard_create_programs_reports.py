from trytond.wizard import Wizard, StateView, Button, StateTransition, StateAction
from trytond.model import ModelView, fields
from trytond.transaction import Transaction
from trytond.pool import Pool 
from trytond.pyson import Eval, Not, Bool, Equal

import csv
import sys
from io import StringIO
from datetime import datetime, date, time, timedelta

__all__ = ['CreateProgramsReportsStart',
           'CreateProgramsReportsWizard']
# modelview es de vista


class CreateProgramsReportsStart(ModelView):    
    'Programs Report Start'
    __name__ = 'programs.reports.start'
    
    report_ = fields.Selection([
        (None,''),
        ('create_remediar_report','Programa REMEDIAR'),
        ('create_leche_report','Dir. Materno Infanto Juvenil (Entrega de Leche)'),
        ('create_salud_sexual_report','Programa Salud Sexual y Reproductiva'),
         ],'Report',required=True,sort=False)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)


class CreateProgramsReportsWizard(Wizard):
    'Programs Report Wizard'
    __name__ = 'programs.reports.wizard'
    
    @classmethod
    def __setup__(cls):
        super(CreateProgramsReportsWizard,cls).__setup__()
        cls._error_messages.update({
            'end_date_before_start_date': 'The end date cannot be major thant the start date',
            })
    
    start = StateView('programs.reports.start',
                      'health_programs_fiuner.create_programs_report_start_view',[
                        Button('Cancel','end','tryton-cancel'),
                        Button('Print Report','prevalidate','tryton-ok',default=True),
                       ])
    
    prevalidate = StateTransition()
    
    create_leche_report =\
        StateAction('health_programs_fiuner.act_gnuhealth_programs_leche_report')
    
    create_salud_sexual_report =\
        StateAction('health_programs_fiuner.act_gnuhealth_programs_salud_sexual_report')
    
    create_remediar_report =\
        StateAction('health_programs_fiuner.act_gnuhealth_programs_remediar_report')
    
    
    def default_start(self,fields):
        return{
            'end_date':datetime.now()
            }
    
    def transition_prevalidate(self):
        if self.start.end_date < self.start.start_date:
            self.raise_user_error('end_date_before_start_date')
        return self.start.report_

    def fill_data(self):
        start = self.start.start_date
        end = self.start.end_date
        return {
            'start':start, 
            'end':end 
            }
        
    def do_create_leche_report(self, action):
        data = self.fill_data()
        return action, data

    def do_create_salud_sexual_report(self, action):
        data = self.fill_data()
        return action, data
    
    def do_create_remediar_report(self, action):
        data = self.fill_data()
        return action, data
    
