# -*- coding: utf-8 -*-

from trytond.pool import Pool
from trytond.report import Report

from datetime import datetime
from dateutil.relativedelta import relativedelta

__all__ = ['ProgramsRemediarReport']

class ProgramsRemediarReport(Report):
    'Prgogram Remediar Report'
    __name__ = 'programs.remediar.reports'


    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        Moves = pool.get('stock.move')
        Medicaments = pool.get('gnuhealth.medicament')
        
        
        context = super(ProgramsRemediarReport, cls).get_context(records, data)
        if data:
            start = data['start']
            end = data['end']
            moves = Moves.search([
                                                ('effective_date','>=',start),
                                                ('effective_date','<=',end),
                                                ('to_location.name','=','Cliente'),
                                                ])

            inputs = Moves.search([
                                                ('effective_date','>=',start),
                                                ('effective_date','<=',end),
                                                ('from_location.name','=','Proveedor'),
                                                ])

            context['objects'] = moves
           
            remediar = Medicaments.search([('category.name','=','REMEDIAR' )])
            
            context['objects1'] = remediar
            
            context['data'] = []
            
            for x in remediar:
               
                aux=[]
                aux.append(x.name.name)
                aux.append(x.name.code)
                aux.append(len([y for y in moves if x.name.code == y.product.code and y.to_location.name == "Cliente"] ))
                aux.append(sum([y.quantity for y in moves if x.name.code == y.product.code and y.to_location.name == "Cliente"] ))
                aux.append(len([y for y in inputs if x.name.code == y.product.code and y.from_location.name == "Proveedor"] ))
                aux.append(sum([y.quantity for y in inputs if x.name.code == y.product.code and y.from_location.name == "Proveedor"] ))
                context['data'].append(aux)

        return context


