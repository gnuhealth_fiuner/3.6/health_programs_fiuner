# -*- coding: utf-8 -*-

from trytond.pool import Pool
from trytond.report import Report

from datetime import datetime
from dateutil.relativedelta import relativedelta

__all__ = ['ProgramsLecheReport']

class ProgramsLecheReport(Report):
    'Programs Report'
    __name__ = 'programs.leche.reports'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        ProgramsMothers = pool.get('programas.leche_materna_madres')
        ProgramsChildren = pool.get('programas.leche_materna_menores')
        HealthInst = Pool().get('gnuhealth.institution')
       # Config = Pool().get('gnuhealth.du.configuration')
       # config = Config (1)
        context = super(ProgramsLecheReport, cls).get_context(records, data) #es un diccionario
        start = data['start']
        end = data['end']
        mothers = ProgramsMothers.search([
                                ('fecha_entrega','>=',start),
                                ('fecha_entrega','<=',end)])
        children = ProgramsChildren.search([
                                ('fecha_entrega','>=',start),
                                ('fecha_entrega','<=',end)])
        context['today'] = datetime.now()
        context['objects'] = mothers
        context['objects1'] = children
        context['children_from_0_to_11_months_rbp_bp_cl'] = {} #esto es un diccionario 
        context['delivery_from_0_to_11_months_rbp_bp_cl'] = {}
        context['children_from_6_to_11_months'] = {}
        context['delivery_from_6_to_11_months'] = {}
        context['children_from_12_to_23_months'] = {}
        context['delivery_from_12_to_23_months'] = {}
        context['children_from_2_to_5_years_rbp_bp_cl'] = {} 
        context['delivery_from_2_to_5_years_rbp_bp_cl'] = {}
        context['children_from_12_to_23_months_rbp_bp_cl'] = {}
        context['delivery_from_12_to_23_months_rbp_bp_cl'] = {} 
        context['number_of_pregnant'] = {}     
        context['delivery_number_of_pregnant'] = {} 
        context['children_from_0_to_11_months'] = {} #esto es un diccionario 
        context['delivery_from_0_to_11_months'] = {}
        context['vulnerables'] = '0'
        context['total_population'] = {}
        context['delivery_total'] = {}
        context['delivery_extra_programs'] = {}
        context['extra_programs'] = {}
        for day in range(1,32):
           context['children_from_6_to_11_months'][str(day)]= len([x for x in children 
                            if (x.edad >= 0.5 and x.edad < 1) and
                                x.fecha_entrega.day == day])
           context['delivery_from_6_to_11_months'][str(day)]= sum([x.cantidad for x in children 
                            if (x.edad >= 0.5 and x.edad < 1) and
                                x.fecha_entrega.day == day])
           context['children_from_0_to_11_months_rbp_bp_cl'][str(day)]= len([x for x in children 
                            if (x.edad >= 0 and x.edad < 1) and
                                x.fecha_entrega.day == day and 
                                x.diag_nut in ['RBP','BP','CL']
                               ])
           context['delivery_from_0_to_11_months_rbp_bp_cl'][str(day)]= sum([x.cantidad for x in children 
                            if (x.edad >= 0 and x.edad < 1) and
                                x.fecha_entrega.day == day and 
                                x.diag_nut in ['RBP','BP','CL']
                               ])
           context['children_from_12_to_23_months'][str(day)]= len([x for x in children 
                            if (x.edad >= 1 and x.edad < 2) and
                                x.fecha_entrega.day == day])
           context['delivery_from_12_to_23_months'][str(day)]= sum([x.cantidad for x in children 
                            if (x.edad >= 1 and x.edad < 2) and
                                x.fecha_entrega.day == day])
           context['children_from_12_to_23_months_rbp_bp_cl'][str(day)]= len([x for x in children 
                            if (x.edad >= 1 and x.edad < 2) and
                                x.fecha_entrega.day == day and 
                                x.diag_nut in ['RBP','BP','CL']
                               ])
           context['delivery_from_12_to_23_months_rbp_bp_cl'][str(day)]= sum([x.cantidad for x in children 
                            if (x.edad >= 1 and x.edad < 2) and
                                x.fecha_entrega.day == day and 
                                x.diag_nut in ['RBP','BP','CL']
                               ])
           context['children_from_2_to_5_years_rbp_bp_cl'][str(day)] = len([x for x in children 
                            if (x.edad >= 2 and x.edad <= 5) and
                                x.fecha_entrega.day == day and 
                                x.diag_nut in ['RBP','BP','CL']
                               ])
           context['delivery_from_2_to_5_years_rbp_bp_cl'][str(day)]= sum([x.cantidad for x in children 
                            if (x.edad >= 2 and x.edad <= 5) and
                                x.fecha_entrega.day == day and 
                                x.diag_nut in ['RBP','BP','CL']
                               ])
           context['number_of_pregnant'][str(day)]= len([x for x in mothers 
                            if (x.sem_embarazo > 0 and x.sem_embarazo <= 42) and
                                x.fecha_entrega.day == day])
           context['delivery_number_of_pregnant'][str(day)]= sum([x.cantidad for x in mothers 
                            if (x.sem_embarazo > 0 and x.sem_embarazo <= 42) and
                                x.fecha_entrega.day == day])
           context['children_from_0_to_11_months'][str(day)]= len([x for x in children 
                            if (x.edad >= 0 and x.edad < 1) and
                                x.fecha_entrega.day == day 
                               ])
           context['delivery_from_0_to_11_months'][str(day)]= sum([x.cantidad for x in children 
                            if (x.edad >= 0 and x.edad < 1) and
                                x.fecha_entrega.day == day 
                               ])
           context['delivery_extra_programs'][str(day)] =\
               sum([x.extra for x in children if x.extra and x.fecha_entrega.day == day]) 
           context['extra_programs'][str(day)] = len([x.extra for x in children 
                                                    if (x.extra and x.extra > 0 and x.fecha_entrega.day == day)]) 
           context['total_population'][str(day)] = len([x for x in children if x.fecha_entrega.day == day]) + \
                                                   len([x for x in mothers if x.fecha_entrega.day == day])
           context['delivery_total'][str(day)] = sum([x.cantidad for x in children if x.fecha_entrega.day == day]) + \
                                                 sum([x.cantidad for x in mothers if x.fecha_entrega.day == day])
        #get institution data: name, department, city, email
        institution = HealthInst.search(['id','=',HealthInst.get_institution()])
        context['institution'] = institution[0].name.name
        context['institution_department'] = ''
        context['institution_city'] = institution[0].name.addresses[0].city or ''
        context['institution_email'] =\
            ', '.join([x.value for x in institution[0].name.contact_mechanisms if x.type == 'email'])
        
        #get report month and year
        context['report_year'] = start.year
        context['report_month'] = start.month
        return context
        
 
