# -*- coding: utf-8 -*-

from trytond.pool import Pool
from trytond.report import Report

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

__all__ = ['ProgramsSaludSexualReport']

class ProgramsSaludSexualReport(Report):
    'Salud Sexual Report'
    __name__ = 'programs.salud_sexual.reports'


    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        Moves = pool.get('stock.move')
        Medicaments = pool.get('gnuhealth.medicament')

        context = super(ProgramsSaludSexualReport, cls).get_context(records, data)
        if data:
            start = data['start']
            end = data['end']
            moves = Moves.search([
                                                ('effective_date','>=',start),
                                                ('effective_date','<=',end)
                                                ])
            comienzo = data['start'] - timedelta(days=365)
            final = data['start'] - timedelta(days=1)
            salud_sexual_viejos = Moves.search([
                                                ('effective_date','>=',comienzo),
                                                ('effective_date','<=',final),
                                                ('to_location.name','=','Cliente'),
                                                ])

        print(len(salud_sexual_viejos))
        product_default = {
        'R069':'R069 ACO (levonorgestrel + EE)',
        'R071':'R071 ACO lactancia (levonorg.)',
        'R072':'R072 ACI  mensual',
        'R074':'R074 075 ACO Emergencia',
        'R075':'R074 075 ACO Emergencia',
        'R076':'R076 DIU T cobre',
        'R080':'R080 Implante Subdermico',
        'R082':'R082 ACI trimestral lactancia',
        'R083':'R083 DIU Multiload',
        'R084':'R084 ACO (Gestodeno + EE)',
        'R085':'R085 ACO lactancia (desogestrel)',
        'R119':'R119 Ciproterona (hormon. TRANS)',
        'R120':'R120 Estradiol gel (hormona TRANS)',
        'R301':'R301 Test Embarazo',
        'R303':'R303 SIU (Mirena)',
        'R998':'R998 Testoviron (hormona TRANS)',
        'R999':'R999 Espironolactona (hor. TRANS)'}

        #context['products_to_report'] = ['R069','R072','R074','R075','R076','R080','R083','R084','R085','R119','R120','R301','R303','R998','R999']
        context['products_to_report'] = list(product_default.keys())

        # necesito comparar para poder ver a quien se le entrega por primera vez, para
        # conocer los que ingresan al programa
        dni_viejos=[]
        for i in salud_sexual_viejos:
            print (i)
            print ('ESTOY ACA')
            dni_viejos.append(i.origin.patient.name.ref)

        # defino los contadores de totales para mujeres y varones nuevos y viejos en el programa
        context['mujeres_new'] = [0,0]
        context['mujeres_old'] = [0,0]
        context['varones_new'] = [0,0]
        context['varones_old'] = [0,0]

        # Defino un vector con los pacientes que debo diferenciar por sexo y edad
        patients_unique = []
        
        for x in moves:
            contador = 0
            for i in patients_unique:
                if x.origin.patient.name.ref in i.origin.patient.name.ref:
                    contador = contador + 1
            if contador == 0:
                patients_unique.append(x)   



        for x in patients_unique:
            if x.product.code in context['products_to_report']:
                if x.origin.patient.name.ref in dni_viejos:
                    if x.origin.patient.gender == 'f':
                        if x.origin.patient.age_float < 20:
                            context['mujeres_old'][0]+=1 
                        else:
                            context['mujeres_old'][1]+=1    
                    else:
                        if x.origin.patient.age_float < 20:
                            context['varones_old'][0]+=1 
                        else:
                            context['varones_old'][1]+=1     
                else:    
                    if x.origin.patient.gender == 'f':
                        if x.origin.patient.age_float < 20:
                            context['mujeres_new'][0]+=1 
                        else:
                            context['mujeres_new'][1]+=1    
                    else:
                        if x.origin.patient.age_float < 20:
                            context['varones_new'][0]+=1 
                        else:
                            context['varones_new'][1]+=1      

        context['objects'] = moves

        remediar = Medicaments.search([('category.name','=','REMEDIAR' )])    

        context['data'] = {}
        context['name'] = {}

        for i in range(0,len(context['products_to_report'])):
            context['data'][context['products_to_report'][i]]=[0,0]

        for x in moves:
            context['name'][x.product.code] = x.product.name
            
            if x.product.code in context['products_to_report']:
                if x.origin.patient.age_float <= 20.0:
                    context['data'][x.product.code][0] = context['data'][x.product.code][0] + 1
                else:
                    context['data'][x.product.code][1] = context['data'][x.product.code][1] + 1
               
        #print(context['data'])
        
        for x in context['products_to_report']:
            if x in context['name']:
                pass
            else:
                context['name'][x] = product_default[x]
        #print(context['name'])

        return context


